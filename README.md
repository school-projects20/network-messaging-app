# Network messaging app

*We did this program for a school exam. This repository is only for demonstration purposes.*

Developers :
- REAL Titouan
- SENGEISSEN Romain

This repository contains a server program that stores conversations as well as user login info, and a client program that proposes to register or login, and send messages to anyone.

## Known flows

This program does not handle unexpected inputs. Please do not ask to use a non-existing file. Please do not use a simulation name that was already used.

# Development

I will rewrite some of this code to make it better and to learn from it.

I will also translate it to English.
