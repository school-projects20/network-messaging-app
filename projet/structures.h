#ifndef STRUCTURES
#define STRUCTURES

#include "constantes.h"

typedef struct thread_client_input
{
    int id ;
    int socket_discussion ;
} Thread_client_input ;

typedef struct identifiants
{
    char pseudo[MAX_CHAR];
    char mdp[MAX_CHAR];
    // bool en_ligne ;
    struct identifiants *suivants ;

} Identifiants ;

typedef struct liste_identifiants
{
	Identifiants *tete ;
} Liste_identifiants ;

typedef struct infos_contact
{
    char pseudo[MAX_CHAR] ;
    // bool nouveau_message ;
    // bool en_ligne ;
} Infos_contact ;

typedef struct message_discussion
{
    char expediteur[MAX_CHAR] ;
    char destinataire[MAX_CHAR] ;
    // char date_heure[MAX_CHAR] ;
    char contenu[MAX_CHAR] ;
    // bool lu ;
    struct message_discussion *suivant ;
} Message_discussion ;

typedef struct liste_messages
{
    Message_discussion *tete ;
} Liste_messages ;

typedef struct thread_live_input
{
    int id ;
    int discussion_id ;
    int socket_discussion ;
    Identifiants *expediteur ;
    Identifiants *destinataire ;
} Thread_live_input ;

typedef struct discussion_live
{
    char nom_1[MAX_CHAR] ;
    char nom_2[MAX_CHAR] ;
} Discussion_live ;

#endif