#include "pse.h"
#include "constantes.h"
#include "structures.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define MACHINE "client"

void black()    {printf("\033[1;30m");}
void red()      {printf("\033[1;31m");}
void green()    {printf("\033[1;32m");}
void yellow ()  {printf("\033[1;33m");}
void blue()     {printf("\033[1;34m");}
void purple()   {printf("\033[1;35m");}
void cyan()     {printf("\033[1;36m");}
void white()    {printf("\033[1;37m");}
void reset()    {printf("\033[0m");}


//void* recevoir_message(void *arg);
void identification(Identifiants *identifiants);

int exchange_socket;


int main(int argc, char *argv[])
{
    if(argc != 3)
    {
        perror("Syntax error: use ./client <IP> <port>");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in *server_address;
    int result;

    exchange_socket = socket(AF_INET, SOCK_STREAM, 0);
    if(exchange_socket < 0)
    {
        perror("Socket creation error: couldn't create an exchange socket.");
        exit(EXIT_FAILURE);
    }

    server_address = resolv(argv[1], argv[2]); //! not std func
    if(server_address == NULL)
    {
        perror("Resolution error: couldn't resolve server address.");
        exit(EXIT_FAILURE);
    }

    green();
    printf("\n\n_______________\n\nConnecting to : \nIP : %s\nPort : %hu\n_______________\n\n\n",
	       stringIP(ntohl(server_address->sin_addr.s_addr)),
	       ntohs(server_address->sin_port));
    reset();

    int connected = connect(exchange_socket, (struct sockaddr *)server_address, sizeof(struct sockaddr_in));
    if(connected < 0)
    {
        perror("Connection error: couldn't connect to the specified server.");
        exit(EXIT_FAILURE);

    }

    // CONNEXION OU INSCRIPTION (ie client connecté)


    printf("Welcome to our messaging system !\n");

    Identifiants *identifiants = (Identifiants *)malloc(sizeof(Identifiants));

    identification(identifiants);

    int stop = 0;

    while (!stop)
    {
        purple();
        printf("\n\n------------------------Contacts------------------------\n\n");
        reset();

        Infos_contact* interlocuteur = (Infos_contact*) malloc(sizeof(Infos_contact));
        result = read(exchange_socket,interlocuteur,sizeof(Infos_contact));
        if(result == -1)
        {
            printf("ERROR : READ\n");
        }

        while(strcmp(interlocuteur->pseudo,"0")!=0)
        {
            if(strcmp(interlocuteur->pseudo,identifiants->pseudo)!=0)
            {
                /* if(ic->nouveau_message)
                {
                    printf("[");
                    yellow();
                    printf("Nouveaux messages");
                    reset();
                    printf("] ");
                }

                if(ic->en_ligne)
                {
                    printf("[");
                    green();
                    printf("Online");
                    reset();
                    printf("] ");
                }
                else
                {
                    printf("[");
                    red();
                    printf("Offline");
                    reset();
                    printf("] ");
                } */

                printf("%s\n", interlocuteur->pseudo);
            }

            result = read(exchange_socket,interlocuteur,sizeof(Infos_contact));
            if(result == -1)
            {
                printf("ERROR : READ\n");
            }

        }


        printf("\n\nDe quel destinataire voulez vous ouvrir la conversation ?\n");
        blue();
        printf("> Tapez le pseudo du destinataire\n\n");
        reset();

        char destinataire[MAX_CHAR];
        Message_discussion m;
        //pthread_t idThread;

        scanf("%s",destinataire);

        if(strcmp(destinataire,"0")!=0)
        {
            purple();
            printf("\n\n----------------Conversation avec %s----------------\n\n",destinataire);
            reset();

            result = write(exchange_socket,destinataire,MAX_CHAR*sizeof(char));
            if(result == -1)
            {
                printf("ERROR : WRITE\n");
            }

            result = read(exchange_socket,&m,sizeof(Message_discussion));
            if(result == -1)
            {
                printf("ERROR : READ\n");
            }

            while(strcmp(m.expediteur,"0")!=0)
            {
                if(strcmp(m.expediteur,identifiants->pseudo)==0)
                {
                    white();
                }
                else
                {
                    reset();
                }

                printf("[%s] : %s\n",m.expediteur,m.contenu);

                result = read(exchange_socket,&m,sizeof(Message_discussion));
                if(result == -1)
                {
                    printf("ERROR : READ\n");
                }
            }

            reset();

        }

        else
        {
            stop = 1;
            result = write(exchange_socket,destinataire,MAX_CHAR*sizeof(char));
            if(result == -1)
            {
                printf("ERROR : WRITE\n");
            }

            if(close(exchange_socket) == -1)
                perror("close socket");
                exit(EXIT_FAILURE);

            exit(EXIT_SUCCESS);
        }

        if(stop == 0)
        {

            Message_discussion message_envoye;
            //pthread_create(&idThread, NULL, recevoir_message,&sock);

            white();
            printf("[%s] : ",identifiants->pseudo);
            scanf(" %[^\n]",message_envoye.contenu);
            reset();

            strcpy(message_envoye.destinataire, destinataire);
            strcpy(message_envoye.expediteur, identifiants->pseudo);
            // strcpy(menv.date_heure,"heure");

            result = write(exchange_socket,&message_envoye,sizeof(Message_discussion));
            if(result == -1)
            {
                printf("ERROR : WRITE\n");
            }


            while(strcmp(message_envoye.contenu,"0")!=0)
            {
                white();
                printf("[%s] : ",identifiants->pseudo);
                scanf(" %[^\n]",message_envoye.contenu);
                reset();

                result = write(exchange_socket,&message_envoye,sizeof(Message_discussion));
                if(result == -1)
                {
                    printf("ERROR : WRITE\n");
                }
            }

            //pthread_cancel(idThread);

        }
    }
}


void identification(Identifiants *identifiants)
{
    int result;
    int reponse;

    printf("Please choose one of the following options.\n");
    blue();
    printf("0. Quit\n");
    printf("1. Log in\n");
    printf("2. Sign in\n");
    reset();


    char choice_char[MAX_CHAR] ;

    scanf("%s", choice_char);

    while (strcmp(choice_char, "0") != 0 && strcmp(choice_char, "1") != 0 && strcmp(choice_char, "2") != 0)
    {
        red();
        printf("Invalid input. Please try again.\n");
        reset();
        printf("Please choose one of the following options.\n");
        blue();
        printf("0. Quit\n");
        printf("1. Log in\n");
        printf("2. Sign in\n");
        reset();
    }

    int choice = atoi(choice_char);

    int write_return_status = write(exchange_socket, &choice, sizeof(int));
    if(write_return_status == -1)
    {
        perror("Socket error: couldn't write into the exchange socket.");
        exit(EXIT_FAILURE);
    }

    if(choice==NOUVEAU_COMPTE)
    {
        account_creation(identifiants);
    }

    if(choice==COMPTE_EXISTANT)
    {
        int echec = 1;

        while(echec == 1)
        {
            blue();
            printf("> Saisissez votre pseudo\n");
            reset();
            scanf("%s",identifiants->pseudo);
            blue();
            printf("> Saisissez votre mot de passe\n");
            reset();
            scanf("%s",identifiants->mdp);

            result = write(exchange_socket,identifiants,sizeof(Identifiants));
            if(result == -1)
            {
                printf("ERROR : WRITE\n");
            }

            result = read(exchange_socket,&reponse,sizeof(int));
            if(result == -1)
            {
                printf("ERROR : READ\n");
            }

            if(reponse==CONNEXION_REFUSEE)
            {
                red();
                printf("\nECHEC DE LA CONNEXION\nPseudo ou mot de passe incorrect\n\n");
                echec = 1;
                reset();
            }
            if(reponse==CONNEXION_ACCEPTEE)
            {
                green();
                printf("\nPseudo et identifiant valides\nVous etes maintenant connecté\n\n");
                echec = 0;
                reset();
            }

        }

    }
}


void account_creation(Identifiants *identifiants)
{
    int created = false;

    while(!created)
    {
        blue();
        printf("> Choose a username. Use only a-z, A-Z and 0-9 characters.\n");
        reset();
        scanf("%s",identifiants->pseudo);
        blue();
        printf("> Choose a password. Use only a-z, A-Z and 0-9 characters.\n");
        reset();
        scanf("%s",identifiants->mdp);

        int write_return_status = write(exchange_socket,identifiants,sizeof(Identifiants));
        if(write_return_status == -1)
        {
            perror("Socket error: couldn't write into the exchange socket.");
            exit(EXIT_FAILURE);
        }

        int account_creation_status;
        int read_return_status = read(exchange_socket, &account_creation_status, sizeof(int));
        if(read_return_status == -1)
        {
            perror("Socket error: couldn't read from the exchange socket.");
            exit(EXIT_FAILURE);
        }

        switch(account_creation_status)
        {
            case COMPTE_CREE_AVEC_SUCCES:
                green();
                printf("\nInscription validée\nVous etes maintenant connecté\n");
                reset();
                created = 1 ;
                break;
            case COMPTE_NON_CREE:
                red();
                printf("\nECHEC DE LA DEMANDE\nL'erreur est probablement parmis celles ci :\n-Compte déjà existant\n-Pseudo ou mot de passe vide ou valant 0\n\n");
                created = 0 ;
                reset();
                break;
        }
    }
}

/* void* recevoir_message(void* Sock)
{
    int* sock = (int*)Sock;
    Message_discussion mrec;

    while(1)
    {
        int ret = read(*sock,&mrec,sizeof(Message_discussion));

        if(ret!=0)
        {
            reset();
            printf("[%s] : %s\n",mrec.expediteur,mrec.contenu);
        }

        if(ret == -1)
        {
            printf("ERROR : READ\n");
        }

    }

    pthread_exit(NULL);
}*/