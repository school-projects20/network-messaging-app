PROJET PSE
REAL Titouan
SENGEISSEN Romain

Ce projet a pour objectif de coder une application client-serveur multithread communicant via des sockets TCP/IP

Le sujet choisi ici est la création d'une messagerie instantanée avec sauvegarde de conversations.

Notre projet dispose des propriétés suivantes :

-système de connexion via identifiant et mot de passe (possibilité de création de compte)
-affichage des contacts
-selection du contact et ouverture de la discussion (contenant les messages echangés précédement)
-ecriture des message et reception


L'organisation de notre programme est la suivante :

Le client est chargé de l'interface utilisateur, de la reception et de l'affichage des données. Il communique avec le serveur via socket. Il s'arrête par CTRL+C.

Le serveur est en charge de la reception des données et de leur stockage. Le stockage des données (identifiants, mot de passe et messages) se fait via fichiers texte. En plus du thread principal, le serveur dispose de 50 threads au maximum pour les connexions clients, en format dynamique, et 50 threads activables pour la discusion instantanée (workers dynamiques limités à 50).

Les echanges entre le client et le serveur est effectué en envoyant des structures de données (structure identifiant contenant mot de passe et pseudo, structure message contenant toutes les informations relatives au message : destinataire, expediteur, contenu...)


GENERATION DU PROJET : se placer dans le fichier projet et exécuter la commande make.
Lancer ensuite ./serveur <port> dans un terminal et ./client <ip_format_decimal_a_point> <port>

/!\ un WARNING est présent lors de la compilation dont nous ne comprenons pas l'origine.
Il est demandé qu'une variable soit cast en struct sockaddr_in *, or c'est bien déjà le cas à notre avis. Ce warning ne semble pas impacter le code.

Notre tentative de mettre à jour les messages en temps réel a échoué, ainsi que celle d'afficher les clients actuellement en ligne et les messages non lus. Une grande partie du code implémentant ces fonctions est présente dans cette version.