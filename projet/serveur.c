#include "pse.h"
#include "constantes.h"
#include "structures.h"
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#define MACHINE "Serveur"
#define MAX_THREADS 50
#define NOM_DATABASE "database.txt"


void *thread_client(void *arg);
void *live_reception(void *arg);
void lire_database();
void ajouter_identifiants(Identifiants *identifiants);
bool comparer_identifiants(Identifiants *identifiants, Identifiants *identifiants_courants);
bool pseudo_est_dans_database(Identifiants *identifiants);
void lire_fichier_messages(int discussion_id, char *nom_1, char *nom_2);
// void ajouter_message(int discussion_id, char *expediteur, char *destinataire, bool lu, char *contenu);
void ajouter_message(int discussion_id, char *expediteur, char *destinataire, char *contenu);
void identification_client(Identifiants *identifiants, int exchange_socket);
void envoyer_info_contacts(Identifiants *identifiants, int exchange_socket);
void ecrire_message_dans_fichier(Identifiants *identifiants, Identifiants *interlocuteur, char *contenu);


sem_t nombre_threads_disponibles;
bool threads_disponibles[MAX_THREADS];
bool discussions_live_libres[MAX_THREADS];
pthread_mutex_t fichier_messages = PTHREAD_MUTEX_INITIALIZER;

Liste_identifiants *liste_identifiants;
Liste_messages listes_pour_discussions[MAX_THREADS];

pthread_t liste_threads_clients[MAX_THREADS];
Thread_client_input thread_inputs[MAX_THREADS];

pthread_t liste_threads_live_conv[MAX_THREADS];
Discussion_live discussions_live[MAX_THREADS];
Thread_live_input thread_live_inputs[MAX_THREADS];

short port;
int socket_ecoute;

int nb_messages_lors_connexion[MAX_THREADS];




int main(int argc, char *argv[])
{
    //* Test entrées
    if(argc != 2)
    {
        perror("Syntax error: use ./server <port>");
        exit(EXIT_FAILURE);
    }

    if(argv[1] < 0 || 65535 < argv[1])
    {
        perror("Syntax error: <port> must be between 0 and 65535.");
        exit(EXIT_FAILURE);
    }


    //* Initialisation variables
    struct sockaddr_in listen_socket;
    listen_socket.sin_family = AF_INET;
    listen_socket.sin_addr.s_addr = INADDR_ANY;
    port = (short)atoi(argv[1]);
    listen_socket.sin_port = htons(port);

    sem_init(&nombre_threads_disponibles, 0, MAX_THREADS);

    for (int i=0; i<MAX_THREADS; i++)
    {
        threads_disponibles[i] = true;
        discussions_live_libres[i] = true;
    }
    liste_identifiants = (Liste_identifiants *)malloc(sizeof(Liste_identifiants));
    liste_identifiants->tete = NULL;

    int result; //Throwaway variable to test function outputs

    lire_database();


    //* Création socket d'écoute
    printf("%s : Création de la socket d'écoute...\n", MACHINE);
    socket_ecoute = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_ecoute < 0)
    {
        erreur_IO("Erreur lors de la création de la socket d'écoute.");
    }
    printf("%s : Socket d'écoute créée.\n", MACHINE);


    //* Liaison socket d'écoute
    printf("%s : Liaison de la socket d'écoute à l'adresse %d:%d...\n", MACHINE, listen_socket.sin_addr.s_addr, port);
    result = bind(socket_ecoute, (struct sockaddr *)&listen_socket, sizeof(listen_socket));
    if (result == -1)
    {
        erreur_IO("Erreur lors de la liaison de la socket d'écoute à l'adresse voulue");
    }
    printf("%s : Socket liée à l'adresse %s:%d.\n", MACHINE, inet_ntoa(listen_socket.sin_addr), port);


    //* Écoute de la socket
    printf("%s : Lancement de l'écoute de la socket...\n", MACHINE);
    result = listen(socket_ecoute, 5);
    if (result == -1)
        erreur_IO("Erreur lors du lancement de l'écoute de la socket");
    printf("%s : Socket en écoute.\n", MACHINE);


    //*Serveur en ligne : Acceptation des connexions
    printf("%s : Serveur en ligne.\n", MACHINE);
    while(1)
    {
        int exchange_socket;
        struct sockaddr_in adresse_client;
        unsigned int longueur_adresse_client;
        int thread_id;

        longueur_adresse_client = sizeof(adresse_client);
        exchange_socket = accept(socket_ecoute, (struct sockaddr * restrict)&adresse_client, &longueur_adresse_client);
        if (exchange_socket == -1)
        {
            erreur_IO("Erreur lors de la création de la socket de discussion");
        }

        sem_wait(&nombre_threads_disponibles);
        while (!threads_disponibles[thread_id])
        {
            thread_id ++;
            thread_id = thread_id%MAX_THREADS;
        }
        pthread_join(liste_threads_clients[thread_id], NULL);

        threads_disponibles[thread_id] = false;
        thread_inputs[thread_id].id = thread_id;
        thread_inputs[thread_id].exchange_socket = exchange_socket;
        pthread_create(&liste_threads_clients[thread_id], NULL, thread_client, &thread_inputs[thread_id]);
    }
    exit(EXIT_SUCCESS);
}



void *thread_client(void *arg)
{
    Thread_client_input *data = (Thread_client_input *)arg;
    int thread_id = data->id;
    int exchange_socket = data->exchange_socket;

    //* Compte existant ou création ? Identification
    Identifiants *identifiants;
    identifiants = (Identifiants *)malloc(sizeof(Identifiants));
    // identifiants->en_ligne = false;
    identification_client(identifiants, exchange_socket);

    bool client_en_ligne = true;
    while(client_en_ligne)
    {
        nb_messages_lors_connexion[thread_id] = 0;
        envoyer_info_contacts(identifiants, exchange_socket);

        Identifiants interlocuteur;
        read(exchange_socket, &interlocuteur.pseudo, MAX_CHAR*sizeof(char));
        if (strcmp(interlocuteur.pseudo, "0") != 0)
        {
            int discussion_id = 0;
            for (int i=0; i<MAX_THREADS; i++)
            {
                if (((strcmp(identifiants->pseudo, discussions_live[i].nom_1) == 0) && (strcmp(interlocuteur.pseudo, discussions_live[i].nom_2) == 0)) || ((strcmp(identifiants->pseudo, discussions_live[i].nom_2) == 0) && (strcmp(interlocuteur.pseudo, discussions_live[i].nom_1) == 0)))
                {
                    break;
                }
                discussion_id ++;
            }
            if (discussion_id == MAX_THREADS)
            {
                for (int i=0; i<MAX_THREADS; i++)
                {
                    if (discussions_live_libres[i])
                    {
                        discussion_id = i;
                        discussions_live_libres[i] = false;
                        break;
                    }
                }
            }
            strcpy(discussions_live[discussion_id].nom_1, identifiants->pseudo);
            strcpy(discussions_live[discussion_id].nom_2, interlocuteur.pseudo);
            listes_pour_discussions[discussion_id].tete = NULL;
            pthread_mutex_lock(&fichier_messages);
            lire_fichier_messages(discussion_id, identifiants->pseudo, interlocuteur.pseudo);
            pthread_mutex_unlock(&fichier_messages);

            Message_discussion *message_courant;
            message_courant = listes_pour_discussions[discussion_id].tete;
            while (message_courant != NULL)
            {
                write(exchange_socket, message_courant, sizeof(Message_discussion));
                message_courant = message_courant->suivant;
                nb_messages_lors_connexion[thread_id] ++;
            }
            Message_discussion message_fin;
            strcpy(message_fin.expediteur, "0");
            write(exchange_socket, &message_fin, sizeof(Message_discussion));

            thread_live_inputs[thread_id].id = thread_id;
            thread_live_inputs[thread_id].discussion_id = discussion_id;
            thread_live_inputs[thread_id].expediteur = identifiants;
            thread_live_inputs[thread_id].destinataire = &interlocuteur;
            thread_live_inputs[thread_id].exchange_socket = exchange_socket;
            // pthread_create(&liste_threads_live_conv[thread_id], NULL, live_reception, &thread_live_inputs[thread_id]); //!unfinished

            Message_discussion message_recu;
            read(exchange_socket, &message_recu, sizeof(Message_discussion));

            while (strcmp(message_recu.contenu, "0") != 0)
            {
                // ajouter_message(thread_id, identifiants->pseudo, interlocuteur.pseudo, false, message_recu.contenu);
                ajouter_message(thread_id, identifiants->pseudo, interlocuteur.pseudo, message_recu.contenu);
                pthread_mutex_lock(&fichier_messages);
                ecrire_message_dans_fichier(identifiants, &interlocuteur, message_recu.contenu);
                pthread_mutex_unlock(&fichier_messages);
                read(exchange_socket, &message_recu, sizeof(Message_discussion));
            }
            // pthread_cancel(liste_threads_live_conv[thread_id]);
        }
        else
        {
            client_en_ligne = false;
        }
    }

    printf("fin\n");
    // identifiants->en_ligne = false;
    threads_disponibles[thread_id] = true;
    pthread_exit(NULL);
}

void identification_client(Identifiants *identifiants, int exchange_socket)
{
    int choix;
    int reponse_demande;
    read(exchange_socket, &choix, sizeof(int));
    if (choix == COMPTE_EXISTANT)
    {
        read(exchange_socket, identifiants, sizeof(Identifiants));
        bool identifiants_valides = false;
        Identifiants *identifiants_courants = liste_identifiants->tete;
        while(identifiants_courants != NULL)
        {
            identifiants_valides = identifiants_valides || comparer_identifiants(identifiants, identifiants_courants);
            identifiants_courants = identifiants_courants->suivants;
        }

        while (!identifiants_valides)
        {
            reponse_demande = CONNEXION_REFUSEE;
            write(exchange_socket, &reponse_demande, sizeof(int));
            read(exchange_socket, identifiants, sizeof(Identifiants));
            identifiants_courants = liste_identifiants->tete;
            while(identifiants_courants != NULL)
            {
                identifiants_valides = identifiants_valides || comparer_identifiants(identifiants, identifiants_courants);
                identifiants_courants = identifiants_courants->suivants;
            }
        }
        reponse_demande = CONNEXION_ACCEPTEE;
        write(exchange_socket, &reponse_demande, sizeof(reponse_demande));
        // identifiants->en_ligne = true;
    }
    else if (choix == NOUVEAU_COMPTE)
    {
        read(exchange_socket, identifiants, sizeof(Identifiants));
        while (pseudo_est_dans_database(identifiants) && (strcmp(identifiants->pseudo, "") != 0) && (strcmp(identifiants->pseudo, "0") != 0) && (strcmp(identifiants->mdp, "") != 0) && (strcmp(identifiants->mdp, "0") != 0))
        {
            reponse_demande = COMPTE_NON_CREE;
            write(exchange_socket, &reponse_demande, sizeof(reponse_demande));
            read(exchange_socket, identifiants, sizeof(Identifiants));
        }
        Identifiants *nouveaux_identifiants = (Identifiants *)malloc(sizeof(Identifiants));
        strcpy(nouveaux_identifiants->pseudo, identifiants->pseudo);
        strcpy(nouveaux_identifiants->mdp, identifiants->mdp);
        ajouter_identifiants(nouveaux_identifiants);
        reponse_demande = COMPTE_CREE_AVEC_SUCCES;
        write(exchange_socket, &reponse_demande, sizeof(reponse_demande));
        // identifiants->en_ligne = true;
        FILE *fichier;
        fichier = fopen(NOM_DATABASE, "a");
        if (fichier != NULL)
        {
            fprintf(fichier, "\n%s %s", nouveaux_identifiants->pseudo, nouveaux_identifiants->mdp);
        }
        fclose(fichier);
    }
}

void envoyer_info_contacts(Identifiants *identifiants, int exchange_socket)
{
    Infos_contact infos_contact;
    Identifiants *identifiants_courants = liste_identifiants->tete;
    while(identifiants_courants != NULL)
    {
        strcpy(infos_contact.pseudo, identifiants_courants->pseudo);
        // infos_contact.nouveau_message = false;
        // infos_contact.en_ligne = identifiants_courants->en_ligne;
        write(exchange_socket, &infos_contact, sizeof(Infos_contact));
        identifiants_courants = identifiants_courants->suivants;
    }
    strcpy(infos_contact.pseudo, "0");
    // infos_contact.en_ligne = false;
    // infos_contact.nouveau_message = false;
    write(exchange_socket, &infos_contact, sizeof(Infos_contact));
}

void ecrire_message_dans_fichier(Identifiants *identifiants, Identifiants *interlocuteur, char *contenu)
{
    char filename1[MAX_CHAR];
    strcpy(filename1, "conversations/");
    strcat(filename1, identifiants->pseudo);
    strcat(filename1, "_");
    strcat(filename1, interlocuteur->pseudo);
    strcat(filename1, ".txt");
    FILE *fichier;
    fichier = fopen(filename1, "a");
    if (fichier != NULL)
    {
        fprintf(fichier, "%s\n", identifiants->pseudo);
        // fprintf(fichier, "%d\n", 0);
        fprintf(fichier, "%s", contenu);
        fprintf(fichier, "\n");

        fclose(fichier);
    }

    char filename2[MAX_CHAR];
    strcpy(filename2, "conversations/");
    strcat(filename2, interlocuteur->pseudo);
    strcat(filename2, "_");
    strcat(filename2, identifiants->pseudo);
    strcat(filename2, ".txt");
    fichier = fopen(filename2, "a");
    if (fichier != NULL)
    {
        fprintf(fichier, "%s\n", identifiants->pseudo);
        // fprintf(fichier, "%d\n", 0);
        fprintf(fichier, "%s", contenu);
        fprintf(fichier, "\n");

        fclose(fichier);
    }
}

// void *live_reception(void *arg)
// {
//     Thread_live_input *data_live = (Thread_live_input *)arg;
//     Message_discussion *message_courant = listes_pour_discussions[data_live->discussion_id].tete;

//     printf("nb mes %d\n", nb_messages_lors_connexion[data_live->id]);

//     for (int i=0; i<nb_messages_lors_connexion[data_live->id]-1; i++)
//     {
//         message_courant = message_courant->suivant;
//     }
//     printf("a\n");
//     fflush(stdout);
//     while(1)
//     {
//         if (message_courant->suivant != NULL)
//         {
//             message_courant = message_courant->suivant;
//             printf("b\n");
//             if (strcmp(message_courant->expediteur, data_live->expediteur->pseudo) != 0)
//             {
//                 printf("sent live : %s a %s, %s\n", message_courant->expediteur, message_courant->destinataire, message_courant->contenu);
//                 printf("verif : %s a %s", data_live->expediteur->pseudo, data_live->destinataire->pseudo);
//                 write(data_live->exchange_socket, &message_courant, sizeof(Message_discussion));
//             }

//         }
//     }

//     pthread_exit(NULL);
// }


void lire_database()
{
	char* pseudo;
	char* mdp;
	pseudo = (char *)malloc(MAX_CHAR*sizeof(char));
	mdp = (char *)malloc(MAX_CHAR*sizeof(char));

	FILE *fichier;
	fichier = fopen(NOM_DATABASE, "r");

	if (fichier != NULL)
	{
		while (fscanf(fichier, "%s %s", pseudo, mdp) == 2)
		{
			Identifiants *identifiants;
			identifiants = (Identifiants *)malloc(sizeof(Identifiants));

			strcpy(identifiants->pseudo, pseudo);
			strcpy(identifiants->mdp, mdp);

			ajouter_identifiants(identifiants);
		}
	}
	fclose(fichier);

	free(pseudo);
    free(mdp);
}

void ajouter_identifiants(Identifiants *identifiants)
{
    identifiants->suivants = liste_identifiants->tete;
    liste_identifiants->tete = identifiants;
}

bool comparer_identifiants(Identifiants *identifiants, Identifiants *identifiants_courants)
{
    return ((strcmp(identifiants->pseudo, identifiants_courants->pseudo) == 0) && (strcmp(identifiants->mdp, identifiants_courants->mdp) == 0));
}

bool pseudo_est_dans_database(Identifiants *identifiants)
{
    bool identifiants_deja_utilises = false;
    Identifiants *identifiants_courants = liste_identifiants->tete;
    while(identifiants_courants != NULL)
    {
        bool meme_pseudo = (strcmp(identifiants->pseudo, identifiants_courants->pseudo)==0);
        identifiants_deja_utilises = identifiants_deja_utilises || meme_pseudo;
        identifiants_courants = identifiants_courants->suivants;
    }
    return identifiants_deja_utilises;
}

void lire_fichier_messages(int discussion_id, char *nom_1, char *nom_2)
{
    FILE *fichier;
    char nom_fichier[MAX_CHAR];
    strcpy(nom_fichier, "conversations/");
    strcat(nom_fichier, nom_1);
    strcat(nom_fichier, "_");
    strcat(nom_fichier, nom_2);
    strcat(nom_fichier, ".txt");
    fichier = fopen(nom_fichier, "r");
    char expediteur[MAX_CHAR];
    // char lu_str[MAX_CHAR];
    // bool lu;
    char contenu[MAX_CHAR];

    if (fichier != NULL)
    {
        while (fgets(expediteur, sizeof(expediteur), fichier) != NULL)
        {
            // fgets(lu_str, sizeof(lu_str), fichier);
            // int k = 0;
            // while (lu_str[k] != '\n')
            // {
            //     k++;
            // }
            // lu_str[k] = 0;
            // lu = atoi(lu_str);
            fgets(contenu, sizeof(contenu), fichier);
            int i = 0;
            while (expediteur[i] != '\n')
            {
                i++;
            }
            expediteur[i] = 0;
            i = 0;
            while (contenu[i] != '\n')
            {
                i++;
            }
            contenu[i] = 0;
            // ajouter_message(discussion_id, expediteur, nom_2, lu, contenu);
            ajouter_message(discussion_id, expediteur, nom_2, contenu);
        }
    }
}

// void ajouter_message(int discussion_id, char *expediteur, char *destinataire, bool lu, char *contenu)
void ajouter_message(int discussion_id, char *expediteur, char *destinataire, char *contenu)
{
    Message_discussion *message;
    message = (Message_discussion *)malloc(sizeof(Message_discussion));
    strcpy(message->expediteur, expediteur);
    strcpy(message->destinataire, destinataire);
    // message->lu = lu;
    strcpy(message->contenu, contenu);
    message->suivant = NULL;

    Message_discussion *message_courant = listes_pour_discussions[discussion_id].tete;
    if (message_courant == NULL)
    {
        listes_pour_discussions[discussion_id].tete = message;
    }
    else
    {
        while(message_courant->suivant != NULL)
        {
            message_courant = message_courant->suivant;
        }
        message_courant->suivant = message;
    }
}